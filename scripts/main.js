
$(function() {

    // Populate using an object literal

    // WTF.init({

    //     heading: [
    //         "It's a forking",
    //         "Check this shirt out, a forking"
    //     ],
    //     response: [
    //         "Already forking seen one",
    //         "Give me a-forking-nother one",
    //         "Don't really give a fork"
    //     ],
    //     template: [
    //         "Big @color @animal",
    //         "Silly @animal with @color fur"
    //     ],
    //     animal: [
    //         "dog",
    //         "cat",
    //         "rabbit"
    //     ],
    //     color: [
    //         "red",
    //         "blue",
    //         "green",
    //         "yellow"
    //     ]
    // });

    // Populate using a JSON file
    // WTF.init( 'sample.json' );

    // Populate using a Google spreadsheet ID (you must publish it first!)
    // @see https://support.google.com/drive/answer/37579?hl=en
    WTF.init( '1RF51na8fYGV9jqWdVjEKfeGxp-yYoUOeKwbj6ySmTNE' );

});
